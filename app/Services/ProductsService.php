<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Product;
use App\Models\Subcategory;

class ProductsService {

    public function categories() {
        return Category::all();
    }

    public function subcategoriesOfCategory($category_id) {
        $category = Category::find($category_id);
        $subcategory_ids = $category->subcategories()->pluck('subcategory_id');
        return Subcategory::all()->whereIn('id', $subcategory_ids);
    }

    public function productsOfSubcategory($category_id, $subcategory_id) {
        $subcategory = Subcategory::find($subcategory_id);
        $ids = $subcategory->products()->pluck('product_id');
        return Product::all()->whereIn('id', $ids);
    }

    public function product($category_id, $subcategory_id, $product_id) {
        return Product::find($product_id);
    }
}
