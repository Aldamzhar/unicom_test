<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Subcategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function products(): BelongsToMany {
        return $this->belongsToMany(Product::class, 'ref_products_subcategories');
    }

    public function categories(): BelongsToMany {
        return $this->belongsToMany(Category::class, 'ref_categories_sub');
    }

    public function getProductsAttribute() {
        $ids = $this->products()->pluck('product_id');
        return Product::all()->whereIn('id', $ids);
    }

    protected $table = 'subcategories';
}
