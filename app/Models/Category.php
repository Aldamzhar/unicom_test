<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    protected $table = 'categories';

    public function subcategories(): BelongsToMany {
        return $this->belongsToMany(Subcategory::class, 'ref_categories_sub');
    }

    public function getSubcategoriesAttribute() {
        $ids = $this->subcategories()->pluck('subcategory_id');
        return Subcategory::all()->whereIn('id', $ids);
    }
}
