<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'description',
        'img'
    ];

    public function subcategories(): BelongsToMany {
        return $this->belongsToMany(Subcategory::class, 'ref_products_subcategories');
    }

    public function getStorageLinkAttribute($str) {
        return Storage::disk(config('voyager.storage.disk'))->url($str);
    }

    protected $table = 'products';
}
