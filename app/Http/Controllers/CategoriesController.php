<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoriesResource;
use App\Http\Resources\ProductsResource;
use App\Http\Resources\SubcategoriesResource;
use App\Models\Category;
use App\Services\ProductsService;

class CategoriesController extends Controller
{
    private ProductsService $productsService;

    public function __construct(ProductsService $productsService)
    {
        return $this->productsService = $productsService;
    }

    public function categories() {
        return CategoriesResource::collection($this->productsService->categories());
    }

    public function subcategoriesOfCategory($category_id) {
        return SubcategoriesResource::collection($this->productsService->subcategoriesOfCategory($category_id));
    }

    public function productsOfSubcategory($category_id, $subcategory_id) {
        return ProductsResource::collection($this->productsService->productsOfSubcategory($category_id,$subcategory_id));
    }

    public function product($category_id, $subcategory_id, $product_id) {
        return new ProductsResource($this->productsService->product($category_id,$subcategory_id, $product_id));
    }
}
