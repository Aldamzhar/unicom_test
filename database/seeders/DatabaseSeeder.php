<?php

namespace Database\Seeders;

use Database\Seeders\basis\DataRowsTableSeeder;
use Database\Seeders\basis\DataTypesTableSeeder;
use Database\Seeders\basis\MenuItemsTableSeeder;
use Database\Seeders\basis\MenusTableSeeder;
use Database\Seeders\basis\PermissionRoleTableSeeder;
use Database\Seeders\basis\PermissionsTableSeeder;
use Database\Seeders\basis\RolesTableSeeder;
use Database\Seeders\basis\SettingsTableSeeder;
use Database\Seeders\bread\CategoriesTableSeeder;
use Database\Seeders\bread\ProductsTableSeeder;
use Database\Seeders\bread\SubcategoriesTableSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->truncateVoyagerSeed();
        $this->callAppTables();
        $this->callSystemTables();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    private function truncateVoyagerSeed()
    {
        DB::table('menu_items')->truncate();
        DB::table('settings')->truncate();
        DB::table('permissions')->truncate();
        DB::table('permission_role')->truncate();
        DB::table('menus')->truncate();
        DB::table('data_rows')->truncate();
        DB::table('data_types')->truncate();
    }

    public function callSystemTables() {
        $this->call([
            DataTypesTableSeeder::class,
            DataRowsTableSeeder::class,
            MenusTableSeeder::class,
            MenuItemsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionsTableSeeder::class,
            PermissionRoleTableSeeder::class,
            SettingsTableSeeder::class,
        ]);
    }

    public function callAppTables() {
        $this->call([
            ProductsTableSeeder::class,
            SubcategoriesTableSeeder::class,
            CategoriesTableSeeder::class
        ]);
    }
}

