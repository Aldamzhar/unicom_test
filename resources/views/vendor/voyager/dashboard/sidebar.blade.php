<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header" style="background-color: #AABCD1;">

                <a class="navbar-brand" href="{{ route('voyager.dashboard') }}">
                    <div class="logo-icon-container">
                        <img src="/voyager-assets/images/chair.png" alt="Logo Icon">
                    </div>
                    <div class="title" style="font-size: 16px;  text-align: left; margin-left: 0px; color: white; font-family: Monotype,cursive; font-weight: bold;">{{'Система товаров'}}</div>
                </a>
            </div>
        </div>
        <div id="adminmenu">
            <admin-menu :items="{{ menu('admin', '_json') }}"></admin-menu>
        </div>
    </nav>
</div>

